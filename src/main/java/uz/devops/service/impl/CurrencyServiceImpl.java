package uz.devops.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.devops.domain.Currency;
import uz.devops.helper.CurrencyHelper;
import uz.devops.repository.CurrencyRepository;
import uz.devops.service.CurrencyService;
import uz.devops.service.dto.CurrencyDTO;
import uz.devops.service.dto.ResponseDTO;
import uz.devops.service.mapper.CurrencyMapper;
import uz.devops.service.mapper.CurrencyResponseMapper;

@Service
@Transactional
public class CurrencyServiceImpl implements CurrencyService {

    private final Logger log = LoggerFactory.getLogger(CurrencyServiceImpl.class);

    private final CurrencyRepository currencyRepository;

    private final CurrencyMapper currencyMapper;
    private final CurrencyResponseMapper currencyResponseMapper;

    private final CurrencyHelper currencyHelper;

    public CurrencyServiceImpl(
        CurrencyRepository currencyRepository,
        CurrencyMapper currencyMapper,
        CurrencyResponseMapper currencyResponseMapper,
        CurrencyHelper currencyHelper
    ) {
        this.currencyRepository = currencyRepository;
        this.currencyMapper = currencyMapper;
        this.currencyResponseMapper = currencyResponseMapper;
        this.currencyHelper = currencyHelper;
    }

    @Override
    public CurrencyDTO save(CurrencyDTO currencyDTO) {
        log.debug("Request to save Currency : {}", currencyDTO);
        Currency currency = currencyMapper.toEntity(currencyDTO);
        currency = currencyRepository.save(currency);
        return currencyMapper.toDto(currency);
    }

    @Override
    public List<CurrencyDTO> saveAll() {
        List<CurrencyDTO> dtoList = currencyHelper.getCurrenciesFromCbu();
        log.debug("Request to save Currency : {}", dtoList);
        List<Currency> currencies = currencyMapper.toEntity(dtoList);
        currencies = currencyRepository.saveAll(currencies);
        return currencyMapper.toDto(currencies);
    }

    @Override
    public CurrencyDTO update(CurrencyDTO currencyDTO) {
        log.debug("Request to save Currency : {}", currencyDTO);
        Currency currency = currencyMapper.toEntity(currencyDTO);
        currency = currencyRepository.save(currency);
        return currencyMapper.toDto(currency);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ResponseDTO> findAll() {
        log.debug("Request to get all Currencies");
        return currencyRepository.findAll().stream().map(currencyResponseMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ResponseDTO> findOne(Long id) {
        log.debug("Request to get Currency : {}", id);
        return currencyRepository.findById(id).map(currencyResponseMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Currency : {}", id);
        currencyRepository.deleteById(id);
    }
}
