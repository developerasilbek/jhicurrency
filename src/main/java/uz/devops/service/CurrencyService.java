package uz.devops.service;

import java.util.List;
import java.util.Optional;
import uz.devops.service.dto.CurrencyDTO;
import uz.devops.service.dto.ResponseDTO;

public interface CurrencyService {
    CurrencyDTO save(CurrencyDTO currencyDTO);

    List<CurrencyDTO> saveAll();

    CurrencyDTO update(CurrencyDTO currencyDTO);

    List<ResponseDTO> findAll();

    Optional<ResponseDTO> findOne(Long id);

    void delete(Long id);
}
