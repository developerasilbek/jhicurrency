package uz.devops.service.mapper;

import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import uz.devops.domain.Currency;
import uz.devops.service.dto.ResponseDTO;

@Mapper(componentModel = "spring")
public interface CurrencyResponseMapper {
    @Mapping(target = "date", source = "date", dateFormat = "dd.MM.yyyy")
    Currency toEntity(ResponseDTO responseDTO);

    @Mapping(target = "date", source = "date", dateFormat = "dd.MM.yyyy")
    ResponseDTO toDto(Currency currency);

    List<Currency> toEntity(List<ResponseDTO> dtoList);

    List<ResponseDTO> toDto(List<Currency> entityList);
}
