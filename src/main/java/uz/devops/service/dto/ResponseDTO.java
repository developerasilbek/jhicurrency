package uz.devops.service.dto;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;

public class ResponseDTO {

    private Long id;

    private Integer cbuId;

    private Integer code;

    private String name;

    private String nameRu;

    private String nameUz;

    private String nameUzc;

    private String nameEn;

    private Integer nominal;

    private Double rate;

    private Double diff;

    private String date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCbuId() {
        return cbuId;
    }

    public void setCbuId(Integer cbuId) {
        this.cbuId = cbuId;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getNameUz() {
        return nameUz;
    }

    public void setNameUz(String nameUz) {
        this.nameUz = nameUz;
    }

    public String getNameUzc() {
        return nameUzc;
    }

    public void setNameUzc(String nameUzc) {
        this.nameUzc = nameUzc;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public Integer getNominal() {
        return nominal;
    }

    public void setNominal(Integer nominal) {
        this.nominal = nominal;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Double getDiff() {
        return diff;
    }

    public void setDiff(Double diff) {
        this.diff = diff;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return (
            "ResponseDTO{" +
            "id=" +
            id +
            ", cbuId=" +
            cbuId +
            ", code=" +
            code +
            ", name='" +
            name +
            '\'' +
            ", nameRu='" +
            nameRu +
            '\'' +
            ", nameUz='" +
            nameUz +
            '\'' +
            ", nameUzc='" +
            nameUzc +
            '\'' +
            ", nameEn='" +
            nameEn +
            '\'' +
            ", nominal=" +
            nominal +
            ", rate=" +
            rate +
            ", diff=" +
            diff +
            ", date='" +
            date +
            '\'' +
            '}'
        );
    }
}
