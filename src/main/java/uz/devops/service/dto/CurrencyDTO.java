package uz.devops.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.*;

public class CurrencyDTO implements Serializable {

    @JsonProperty("id")
    private Integer cbuId;

    @JsonProperty("Code")
    private Integer code;

    @JsonProperty("Ccy")
    private String name;

    @JsonProperty("CcyNm_RU")
    private String nameRu;

    @JsonProperty("CcyNm_UZ")
    private String nameUz;

    @JsonProperty("CcyNm_UZC")
    private String nameUzc;

    @JsonProperty("CcyNm_EN")
    private String nameEn;

    @JsonProperty("Nominal")
    private Integer nominal;

    @JsonProperty("Rate")
    private Double rate;

    @JsonProperty("Diff")
    private Double diff;

    @JsonProperty("Date")
    private String date;

    public Integer getCbuId() {
        return cbuId;
    }

    public void setCbuId(Integer cbuId) {
        this.cbuId = cbuId;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getNameUz() {
        return nameUz;
    }

    public void setNameUz(String nameUz) {
        this.nameUz = nameUz;
    }

    public String getNameUzc() {
        return nameUzc;
    }

    public void setNameUzc(String nameUzc) {
        this.nameUzc = nameUzc;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public Integer getNominal() {
        return nominal;
    }

    public void setNominal(Integer nominal) {
        this.nominal = nominal;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Double getDiff() {
        return diff;
    }

    public void setDiff(Double diff) {
        this.diff = diff;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CurrencyDTO{" +
            "cbuId=" + getCbuId() +
            ", code=" + getCode() +
            ", name='" + getName() + "'" +
            ", nameRu='" + getNameRu() + "'" +
            ", nameUz='" + getNameUz() + "'" +
            ", nameUzc='" + getNameUzc() + "'" +
            ", nameEn='" + getNameEn() + "'" +
            ", nominal=" + getNominal() +
            ", rate=" + getRate() +
            ", diff=" + getDiff() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
