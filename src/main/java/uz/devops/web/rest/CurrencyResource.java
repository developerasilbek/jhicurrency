package uz.devops.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;
import uz.devops.repository.CurrencyRepository;
import uz.devops.service.CurrencyQueryService;
import uz.devops.service.CurrencyService;
import uz.devops.service.criteria.CurrencyCriteria;
import uz.devops.service.dto.CurrencyDTO;
import uz.devops.service.dto.ResponseDTO;
import uz.devops.web.rest.errors.BadRequestAlertException;

@RestController
@RequestMapping("/api")
public class CurrencyResource {

    private final Logger log = LoggerFactory.getLogger(CurrencyResource.class);

    private static final String ENTITY_NAME = "currency";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CurrencyService currencyService;

    private final CurrencyQueryService currencyQueryService;

    private final CurrencyRepository currencyRepository;

    public CurrencyResource(
        CurrencyService currencyService,
        CurrencyQueryService currencyQueryService,
        CurrencyRepository currencyRepository
    ) {
        this.currencyService = currencyService;
        this.currencyQueryService = currencyQueryService;
        this.currencyRepository = currencyRepository;
    }

    @PostMapping("/currencies")
    public ResponseEntity<List<CurrencyDTO>> createCurrency() throws URISyntaxException {
        List<CurrencyDTO> result = currencyService.saveAll();
        return ResponseEntity
            .created(new URI("/api/saveCurrencies/" + result.size()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.toString()))
            .body(result);
    }

    @PutMapping("/currencies/{id}")
    public ResponseEntity<CurrencyDTO> updateCurrency(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody CurrencyDTO currencyDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Currency : {}, {}", id, currencyDTO);
        if (currencyDTO.getCbuId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, currencyDTO.getCbuId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!currencyRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CurrencyDTO result = currencyService.update(currencyDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, currencyDTO.getCbuId().toString()))
            .body(result);
    }

    @GetMapping("/currencies")
    public ResponseEntity<List<ResponseDTO>> getAllCurrencies(CurrencyCriteria criteria) {
        log.debug("REST request to get Currencies by criteria: {}", criteria);
        List<ResponseDTO> entityList = currencyQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    @GetMapping("/currencies/{id}")
    public ResponseEntity<ResponseDTO> getCurrency(@PathVariable Long id) {
        log.debug("REST request to get Currency : {}", id);
        Optional<ResponseDTO> currencyDTO = currencyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(currencyDTO);
    }

    @DeleteMapping("/currencies/{id}")
    public ResponseEntity<Void> deleteCurrency(@PathVariable Long id) {
        log.debug("REST request to delete Currency : {}", id);
        currencyService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
