export interface ICurrency {
  id?: number;
  cbuId?: number;
  code?: number;
  name?: string;
  nameRu?: string;
  nameUz?: string;
  nameUzc?: string;
  nameEn?: string;
  nominal?: number;
  rate?: number;
  diff?: number;
  date?: string;
}

export class Currency implements ICurrency {
  constructor(
    public id?: number,
    public cbuId?: number,
    public code?: number,
    public name?: string,
    public nameRu?: string,
    public nameUz?: string,
    public nameUzc?: string,
    public nameEn?: string,
    public nominal?: number,
    public rate?: number,
    public diff?: number,
    public date?: string
  ) {}
}

export function getCurrencyIdentifier(currency: ICurrency): number | undefined {
  return currency.id;
}
