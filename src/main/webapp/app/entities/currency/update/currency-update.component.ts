import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ICurrency, Currency } from '../currency.model';
import { CurrencyService } from '../service/currency.service';

@Component({
  selector: 'jhi-currency-update',
  templateUrl: './currency-update.component.html',
})
export class CurrencyUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    cbuId: [null, [Validators.required]],
    code: [null, [Validators.required]],
    name: [null, [Validators.required]],
    nameRu: [null, [Validators.required]],
    nameUz: [null, [Validators.required]],
    nameUzc: [null, [Validators.required]],
    nameEn: [null, [Validators.required]],
    nominal: [null, [Validators.required]],
    rate: [null, [Validators.required]],
    diff: [null, [Validators.required]],
    date: [null, [Validators.required]],
  });

  constructor(protected currencyService: CurrencyService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ currency }) => {
      this.updateForm(currency);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const currency = this.createFromForm();
    if (currency.id !== undefined) {
      this.subscribeToSaveResponse(this.currencyService.update(currency));
    } else {
      this.subscribeToSaveResponse(this.currencyService.create(currency));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICurrency>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(currency: ICurrency): void {
    this.editForm.patchValue({
      id: currency.id,
      cbuId: currency.cbuId,
      code: currency.code,
      name: currency.name,
      nameRu: currency.nameRu,
      nameUz: currency.nameUz,
      nameUzc: currency.nameUzc,
      nameEn: currency.nameEn,
      nominal: currency.nominal,
      rate: currency.rate,
      diff: currency.diff,
      date: currency.date,
    });
  }

  protected createFromForm(): ICurrency {
    return {
      ...new Currency(),
      id: this.editForm.get(['id'])!.value,
      cbuId: this.editForm.get(['cbuId'])!.value,
      code: this.editForm.get(['code'])!.value,
      name: this.editForm.get(['name'])!.value,
      nameRu: this.editForm.get(['nameRu'])!.value,
      nameUz: this.editForm.get(['nameUz'])!.value,
      nameUzc: this.editForm.get(['nameUzc'])!.value,
      nameEn: this.editForm.get(['nameEn'])!.value,
      nominal: this.editForm.get(['nominal'])!.value,
      rate: this.editForm.get(['rate'])!.value,
      diff: this.editForm.get(['diff'])!.value,
      date: this.editForm.get(['date'])!.value,
    };
  }
}
