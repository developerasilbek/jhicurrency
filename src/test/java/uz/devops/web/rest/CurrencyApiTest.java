package uz.devops.web.rest;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import uz.devops.helper.CurrencyHelper;
import uz.devops.service.dto.CurrencyDTO;

@SpringBootTest
public class CurrencyApiTest {

    @Autowired
    private CurrencyResource currencyResource;

    @BeforeEach
    void setupService() {
        //    @MockBean
        CurrencyHelper currencyHelper = Mockito.mock(CurrencyHelper.class);
        List<CurrencyDTO> currencyDTOList = new ArrayList<>();
        CurrencyDTO currencyDTO = new CurrencyDTO();
        currencyDTO.setCbuId(1);
        currencyDTO.setCode(840);
        currencyDTO.setName("USD");
        currencyDTO.setNameRu("Доллар США");
        currencyDTO.setNameUz("AQSH dollari");
        currencyDTO.setNameUzc("АҚШ доллари");
        currencyDTO.setNameEn("US Dollar");
        currencyDTO.setNominal(1);
        currencyDTO.setRate(11284.90);
        currencyDTO.setDiff(9.8);
        currencyDTO.setDate("07.12.2022");
        currencyDTOList.add(currencyDTO);
        Mockito.when(currencyHelper.getCurrenciesFromCbu()).thenReturn(currencyDTOList);
    }

    @Test
    public void getAllCurrenciesAPI() throws URISyntaxException {
        currencyResource.createCurrency();
    }
}
